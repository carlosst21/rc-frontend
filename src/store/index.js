import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      id: null,
      nombre: "",
      apellido: "",
      email: "",
      rut: "",
      access_token: "",
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  },
  getters: {
    getUsuario (state) {
      return state.user;
    }
  }

})
